﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Product.Models
{
    public class ProductRepository
    {
        public List<Product> _products = new List<Product>
        {
            new Product
            {
                Id=1,
                Name = "TV",
                Description = "Great TV"


            },
            new Product
            {
                Id = 2,
                Name = "Radio",
                Description = "Old school Radio",

            },

            new Product{
                Id = 3,
                Name = "Phone",
                Description = "New generation phone"
            }

        };

        public ProductRepository()
        {

        }

        public List<Product> GetAll()
        {
            return _products;
        }

        public Product GetById(int id)
        {
            foreach (var item in _products)
            {
                if (id == item.Id)
                    return item;
            }
            return null;
        }
    }
}
