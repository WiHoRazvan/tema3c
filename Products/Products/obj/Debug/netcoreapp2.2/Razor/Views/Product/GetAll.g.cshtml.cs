#pragma checksum "C:\Users\RJ073934\source\repos\Products\Products\Views\Product\GetAll.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "76ea362318ca11b0a1a4df42b14a133241a9b4a0"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Product_GetAll), @"mvc.1.0.view", @"/Views/Product/GetAll.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Product/GetAll.cshtml", typeof(AspNetCore.Views_Product_GetAll))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"76ea362318ca11b0a1a4df42b14a133241a9b4a0", @"/Views/Product/GetAll.cshtml")]
    public class Views_Product_GetAll : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<Product.Models.Product>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\Users\RJ073934\source\repos\Products\Products\Views\Product\GetAll.cshtml"
  
    ViewData["Title"] = "GetAll";

#line default
#line hidden
            BeginContext(79, 21, true);
            WriteLiteral("\r\n<h1>GetAll</h1>\r\n\r\n");
            EndContext();
#line 8 "C:\Users\RJ073934\source\repos\Products\Products\Views\Product\GetAll.cshtml"
 foreach (var item in Model)
{

#line default
#line hidden
            BeginContext(133, 49, true);
            WriteLiteral("    <table>\r\n    <tr>\r\n        <td>\r\n            ");
            EndContext();
            BeginContext(183, 7, false);
#line 13 "C:\Users\RJ073934\source\repos\Products\Products\Views\Product\GetAll.cshtml"
       Write(item.Id);

#line default
#line hidden
            EndContext();
            BeginContext(190, 43, true);
            WriteLiteral("\r\n        </td>\r\n        <td>\r\n            ");
            EndContext();
            BeginContext(234, 9, false);
#line 16 "C:\Users\RJ073934\source\repos\Products\Products\Views\Product\GetAll.cshtml"
       Write(item.Name);

#line default
#line hidden
            EndContext();
            BeginContext(243, 43, true);
            WriteLiteral("\r\n        </td>\r\n        <td>\r\n            ");
            EndContext();
            BeginContext(287, 16, false);
#line 19 "C:\Users\RJ073934\source\repos\Products\Products\Views\Product\GetAll.cshtml"
       Write(item.Description);

#line default
#line hidden
            EndContext();
            BeginContext(303, 42, true);
            WriteLiteral("\r\n        </td>\r\n    </tr>\r\n    </table>\r\n");
            EndContext();
#line 23 "C:\Users\RJ073934\source\repos\Products\Products\Views\Product\GetAll.cshtml"
}

#line default
#line hidden
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<Product.Models.Product>> Html { get; private set; }
    }
}
#pragma warning restore 1591
