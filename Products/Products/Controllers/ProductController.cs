﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Product.Models;

namespace Products.Controllers
{
    public class ProductController : Controller
    {
        //ProductRepository _productRepository = new ProductRepository();

        //public ProductController(ProductRepository productRepositor)
        //{
        //    _productRepository = productRepositor;
        //}
        public IActionResult GetById()
        {
            
            return View();
        }

        public IActionResult GetAll()
        {
            var repo = new ProductRepository();
            var products = repo._products;
            return View(products);
        }
    }
}