﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TimeAndDate.Models
{
    public class CustomDateTimeRepository
    {
        CustomDateTime CustomDateTime = new CustomDateTime();
        public CustomDateTime GetCurrentDate()
        {
            return new CustomDateTime
            {
                TodaysDate = DateTime.Now
            };
        }
    }
}
