﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TimeAndDate.Models;

namespace TimeAndDate.Controllers
{
    public class CustomDateTimeController : Controller
    {
        //CustomDateTime myDate = new CustomDateTime();

        CustomDateTimeRepository _repo = new CustomDateTimeRepository();

        public IActionResult Index()
        {
            return View(_repo.GetCurrentDate());
            ////var date = myDate.getCurrentTime();
            //return View((object)date);
        }
    }
}