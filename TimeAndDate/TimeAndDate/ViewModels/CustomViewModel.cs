﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimeAndDate.Models;

namespace TimeAndDate.ViewModels
{
    public class CustomViewModel
    {
        public CustomDateTime CustomDateTime { get; set; }
        public string Message { get; set; }
    }
}
